package com.playground.webclient.model;

import java.util.Locale;

public enum ContentType {
    COMMENT, POST;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
