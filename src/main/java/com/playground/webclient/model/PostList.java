package com.playground.webclient.model;

import lombok.Data;

import java.util.List;

@Data
public class PostList {
    private Integer size;
    private List<Post> posts;

    public PostList(List<Post> posts) {
        this.size = posts.size();
        this.posts = posts;
    }
}
