package com.playground.webclient.model;

import lombok.Data;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Data
public class Result<T> {
     ContentType content;
     List<T> items;

    public Result(ContentType content, List<T> items) {
        this.content = content;
        this.items = items;
    }
}
