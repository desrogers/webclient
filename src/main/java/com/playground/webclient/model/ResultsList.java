package com.playground.webclient.model;

import lombok.Data;
import reactor.core.publisher.Mono;

import java.util.List;

@Data
public class ResultsList {
   List<Result> results;

   public ResultsList() {}

   public ResultsList(List<Result> results) {
      this.results = results;
   }
}
