package com.playground.webclient.resources;

import com.playground.webclient.model.Comment;
import com.playground.webclient.model.PostList;
import com.playground.webclient.model.ResultsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@RestController
public class CommentsController {
    @Autowired
    private CommentsService commentsService;
    @Autowired
    private PostsService postsService;

    @RequestMapping(value = "/allComments", method = RequestMethod.GET)
    public ResponseEntity<Flux<Comment>> getAllComments() {
        Flux<Comment> comment = commentsService.getAllComments();
        HttpStatus status = Objects.nonNull(comment) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(comment, status);
    }

    @RequestMapping(value = "/allCommentsForPost/{id}", method = RequestMethod.GET)
    public ResponseEntity<Flux<Comment>> getAllCommentsByPostId(@PathVariable Integer id) {
        Flux<Comment> comments = commentsService.getAllCommentsByPostId(id);
        HttpStatus status = Objects.nonNull(comments) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(comments, status);
    }

    @GetMapping("/all")
    public ResponseEntity<Mono<ResultsList>> getAllContent() {
        Mono<ResultsList> results = postsService.getAllPosts();
        HttpStatus status = Objects.nonNull(results) ? HttpStatus.OK : HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(results, status);
    }
}