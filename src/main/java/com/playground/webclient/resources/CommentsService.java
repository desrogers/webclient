package com.playground.webclient.resources;

import com.playground.webclient.model.Comment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Collections;

@Service
public class CommentsService {

    @Value("${comments.url}")
    private String commentsUrl;

    public Flux<Comment> getAllComments() {
        WebClient client = WebClient
                .builder()
                .baseUrl(commentsUrl)
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap(
                        "url", commentsUrl
                ))
                .build();

        Flux<Comment> commentFlux = client.get()
                .retrieve()
                .bodyToFlux(Comment.class);

        return commentFlux;
    }

    public Flux<Comment> getAllCommentsByPostId(Integer id) {
        WebClient client = WebClient.builder()
                .baseUrl(commentsUrl)
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", commentsUrl))
                .build();

        Flux<Comment> commentFlux = client.get()
                .uri("?postId=" + id)
                .retrieve()
                .bodyToFlux(Comment.class);

        return commentFlux;
    }
}
