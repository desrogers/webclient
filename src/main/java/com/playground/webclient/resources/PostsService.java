package com.playground.webclient.resources;

import com.playground.webclient.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

@Service
public class PostsService {

    @Autowired
    private CommentsService commentsService;

    @Value("${posts.url}")
    String postsUrl;

    public Mono<ResultsList> getAllPosts() {
        Flux<Result> commentsFlux = commentsService
                .getAllComments()
                .buffer()
                .map(comments -> new Result<Comment>(ContentType.COMMENT, comments));

        WebClient client = WebClient
                .builder()
                .baseUrl(postsUrl)
                .defaultCookie("key", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap(
                        "url", postsUrl
                ))
                .build();

        Flux<Result> postFlux = client.get()
                .retrieve()
                .bodyToFlux(Post.class)
                .buffer()
                .map(posts -> new Result<Post>(ContentType.POST, posts));

//        Result<Comment> commentResult = new Result(ContentType.COMMENT, commentsFlux);
//        Result<Post> postResult = new Result(ContentType.POST, postFlux);

        return Flux
                .concat(postFlux, commentsFlux)
                .collectList()
                .map(list -> new ResultsList(list));
    }
}
