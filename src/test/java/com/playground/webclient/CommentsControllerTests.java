package com.playground.webclient;

import com.playground.webclient.model.*;
import com.playground.webclient.resources.CommentsController;
import com.playground.webclient.resources.CommentsService;
import com.playground.webclient.resources.PostsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CommentsController.class)
@Import({CommentsService.class, PostsService.class})
public class CommentsControllerTests {

    @MockBean
    private CommentsService commentsService;

    @MockBean
    private PostsService postsService;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void getAllComments() {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setPostId(1);
        comment.setName("test1@test.com");
        comment.setBody("blah blah blah");

        List<Comment> list = new ArrayList<>();
        list.add(comment);

        Flux<Comment> commentFlux = Flux.fromIterable(list);

        when(commentsService.getAllComments()).thenReturn(commentFlux);

        webTestClient.get()
                .uri("/allComments")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Comment.class);

        verify(commentsService, times(1)).getAllComments();
    }

    @Test
    void getAllCommentsAndPosts() {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setPostId(1);
        comment.setName("test1@test.com");
        comment.setBody("blah blah blah");

        List<Comment> list = new ArrayList<>();
        list.add(comment);

        Post post = new Post();
        post.setId(1);
        post.setUserId(1);
        post.setTitle("test1@test.com");
        post.setBody("blah blah blah");

        List<Post> posts = new ArrayList<>();
        posts.add(post);

        ResultsList resultsList = new ResultsList(Arrays.asList(
                new Result(ContentType.COMMENT, list),
                new Result(ContentType.POST, posts)
        ));

        Flux<Comment> commentFlux = Flux.fromIterable(list);

        when(commentsService.getAllComments()).thenReturn(commentFlux);
        when(postsService.getAllPosts()).thenReturn(Mono.just(resultsList));

        webTestClient.get()
                .uri("/all")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ResultsList.class);

        verify(postsService, times(1)).getAllPosts();
    }

    @Test
    void getAll_withEmptyComments_returnsPostResults() {
        Flux<Comment> emptyFlux = Flux.empty();
        Post post = new Post();
        post.setId(1);
        post.setUserId(1);
        post.setTitle("test1@test.com");
        post.setBody("blah blah blah");

        List<Post> posts = new ArrayList<>();
        posts.add(post);


        ResultsList resultsList = new ResultsList(Arrays.asList(
                new Result(ContentType.COMMENT, new ArrayList())
        ));

        when(postsService.getAllPosts()).thenReturn(Mono.just(resultsList));
        when(commentsService.getAllComments()).thenReturn(emptyFlux);

        webTestClient.get()
                .uri("/all")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.results").isArray()
                .jsonPath("$.results");

        verify(postsService, times(1)).getAllPosts();
    }
}
